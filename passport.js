//passport.js
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const UserModel = require("./models/users/schema");
const bcrypt = require("bcrypt");
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    function(email, password, cb) {
      //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
      return UserModel.findOne({ email })
        .then(user => {
          if (!user) {
            return cb(null, false, {
              message: "Incorrect email or password."
            });
          } else {
            bcrypt
              .compare(password, user.password)
              .then(res => {
                if (res) {
                  return cb(null, user.toJSON(), {
                    message: "Logged In Successfully"
                  });
                } else {
                  return cb(null, false, {
                    message: "Incorrect email or password."
                  });
                }
              })
              .catch(err => console.error(err));
          }
        })
        .catch(err => cb(err));
    }
  )
);

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: "your_jwt_secret"
    },
    function(jwtPayload, cb) {
      //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
      return UserModel.findById(jwtPayload._id)
        .then(user => {
          return cb(null, user);
        })
        .catch(err => {
          return cb(err);
        });
    }
  )
);
