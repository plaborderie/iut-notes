const {passwordChangedMail, passwordResetMail} = require("../../helpers/mail");

const express = require("express");
const router = express.Router();
const User = require("./schema");
const Post = require("../posts/schema");
const bcrypt = require("bcrypt");
const crypto = require('crypto');
const jwt = require("jsonwebtoken");
const passport = require("passport");
const yup = require("yup");
const async = require("async");
require("../../passport");

// get users
router.get("/", (req, res) => {
  User.userlist((err, users) => {
    if (err) throw err;
    res.status(200).send(users);
  });
});

// get user from jwt
router.get(
  "/jwt",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const user = {
      username: req.user.username,
      isAdmin: req.user.isAdmin,
      email: req.user.email
    };
    res.status(200).send(user);
  }
);

// add one new user
router.post("/", (req, res) => {
  const { username, email, password } = req.body;

  // Check security
  const schema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().min(8).required()
  });

  schema.isValid({ email, password })
    .then((valid) => {
      if (valid) {
        // Hash password
        const hashed = bcrypt.hashSync(password, 10);

        // Create user object from credentials
        const user = new User({
          username,
          email,
          password: hashed
        });

        // Saving user!
        user.save((err, newUser) => {
          if (err) {
            console.error(err.code);
            if (err.code === 11000) {
              // User already exists
              res.status(409).send("User exists");
            } else {
              res
                .status(500)
                .send(
                  "Internal server error. User could not be saved to the database."
                );
            }
          } else {
            res.status(200).send("User saved successfully!");
          }
        });
      } else {
        res.status(400).send("Arguments do not match schema");
      }
    });

});

// login
router.post("/login", function(req, res, next) {
  passport.authenticate("local", { session: false }, (err, user, info) => {
    if (err || !user) {
      return res.status(400).json({
        message: "Something is not right",
        user: user
      });
    }
    req.login(user, { session: false }, err => {
      if (err) {
        res.send(err);
      }
      // generate a signed son web token with the contents of user object and return it in the response
      const token = jwt.sign(user, "your_jwt_secret");
      return res.json({ user, token });
    });
  })(req, res);
});

// Reset password
router.post('/forgot', function(req, res, next) {
  async.waterfall([
    (done) => {
      crypto.randomBytes(20, function(err, buf) {
        const token = buf.toString('hex');
        done(err, token);
      });
    },
    (token, done) => {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          res.status(400).send('No account with that email address exists.');
          return err;
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(err => {
          done(err, token, user);
        });
      });
    },
    (token, user, done) => {
      passwordResetMail(user.email, token, req.headers.host);
      res.status(200).send('Reset email sent successfully!');
    }
  ], (err) => {
    if (err) return next(err);
  });
});

router.post('/reset', function(req, res) {
  const {password, token} = req.body;
  async.waterfall([
    (done) => {
      User.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: Date.now() } }, (err, user) => {
        if (!user) {
          res.status(400).send('Password reset token is invalid or has expired.');
          return err;
        }

        user.password = bcrypt.hashSync(password, 10);
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(err => {
          done(err, user);
        });
      });
    },
    (user) => {
      passwordChangedMail(user.email);
      res.status(200).send('Password reset successfully!');
    }
  ], err => {
    console.error(err);
  });
});

// edit user
router.put(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // Check if user is author or admin
    const {user, username} = req.body;
    if (req.user.username !== username) {
      res.status(401).send("Unauthorized.");
    } else {
      // User is authorized
      // Updating user info
      User.findOneAndUpdate(
        {username},
        {username: user.username, email: user.email, password: bcrypt.hashSync(user.password, 10)},
        (err, resultUser) => {
          if (err) {
            console.error("Could not save");
            res.status(500).send(err);
          } else {
            res.status(200).send(resultUser);
          }
        }
      );

      if (username !== user.username) {
        // Update posts author attribute
        Post.updateMany(
          {author: username},
          {author: user.username},
          (err) => {
            if (err) throw err;
          }
        );
      }
    }
  }
);

module.exports = router;
