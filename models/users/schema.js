const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  isAdmin: { type: Boolean, default: false },
  resetPasswordToken: String,
  resetPasswordExpires: Date
});

// Use UserSchema.methods to define functions
UserSchema.statics.userlist = function(cb) {
  this.find()
    .limit(20)
    .exec(function(err, users) {
      if (err) return cb(err);

      cb(null, users);
    });
};

module.exports = mongoose.model("User", UserSchema, "users");
