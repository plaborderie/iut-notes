const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CategorySchema = Schema({
  title: { type: String, required: true },
  semester: { type: String, required: true }
});

module.exports = mongoose.model("Category", CategorySchema, "categories");
