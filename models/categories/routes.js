const express = require("express");
const router = express.Router();
const Category = require("./schema");
const passport = require("passport");
require("../../passport");

// get categories
router.get("/", (req, res) => {
  Category.find({}, (err, categories) => {
    if (err) throw err;
    res.status(200).send(categories);
  }).sort({ title: 1 });
});

// get one category
router.get("/:id", (req, res) => {
  const id = req.params.id;
  Category.findOne({ id }, (err, category) => {
    if (err) throw err;
    res.status(200).send(category);
  });
});

// add one new category
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.isAdmin) {
      // Is an admin
      // Create category object from request
      const category = new Category({
        title: req.body.title
      });

      // Saving category!
      category.save((err, newCategory) => {
        if (err) throw err;
        res
          .status(200)
          .send(`Category ${newCategory.title} saved successfully!`);
      });
    } else {
      // Is not an admin
      res.status(401).send("Unauthorized");
    }
  }
);

module.exports = router;
