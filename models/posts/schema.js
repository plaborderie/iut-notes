const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PostSchema = Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  category: { type: String, required: true },
  author: { type: String, required: true }
});

// Use UserSchema.methods to define functions
PostSchema.statics.postlist = function(cb) {
  this.find().exec(function(err, posts) {
    if (err) return cb(err);

    cb(null, posts);
  });
};

module.exports = mongoose.model("Post", PostSchema, "posts");
