const express = require("express");
const router = express.Router();
const Post = require("./schema");
const passport = require("passport");
require("../../passport");

// get posts
router.get("/", (req, res) => {
  Post.postlist((err, posts) => {
    if (err) throw err;
    res.status(200).send(posts);
  });
});

// get user posts
router.post("/user", (req, res) => {
  const { author } = req.body;
  Post.find({ author }, (err, posts) => {
    if (err) throw err;
    res.status(200).send(posts);
  });
});

// get one post
router.get("/:id", (req, res) => {
  const id = req.params.id;
  Post.findById(id, (err, post) => {
    if (err) throw err;
    res.status(200).send(post);
  });
});

// delete one post
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //Check if admin or author
    const id = req.params.id;
    Post.findById(id, (err, post) => {
      if (err) throw err;
      if (req.user.isAdmin || req.user.username === post.author) {
        // Delete post
        Post.deleteOne(post, (err, deleted) => {
          if (err) throw err;
          res.status(200).send(post);
        });
      } else {
        // Not an admin or author - Unauthorized
        res.status(401).send("Unauthroized");
      }
    });
  }
);

// add one new post
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // Create post object from request
    const { title, content, category } = req.body;
    if (title && content && category) {
      const post = new Post({
        title,
        content,
        category,
        author: req.user.username
      });

      // Saving post!
      post.save((err, newPost) => {
        if (err) throw err;
        res.status(200).send(`Post ${newPost.title} saved successfully!`);
      });
    } else {
      res.status(400).send("Title, content or category is empty");
    }
  }
);

// edit one post
router.put(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // Check if user is author or admin
    if (req.user.username !== req.body.author && !req.user.isAdmin) {
      res.status(401).send("Unauthorized.");
    } else {
      // User is post author or admin: authorized
      const post = new Post({
        title: req.body.title,
        content: req.body.content,
        category: req.body.category,
        author: req.user.username,
        _id: req.body._id
      });

      // Updating post
      Post.findByIdAndUpdate(
        post._id,
        { $set: post },
        { new: true },
        (err, newPost) => {
          if (err) {
            console.error("Could not save");
            res.status(500).send(err);
          } else {
            res.status(200).send(newPost);
          }
        }
      );
    }
  }
);

module.exports = router;
