import EditDeleteNote from './EditDeleteNote';
import {shallow} from 'enzyme';

it('should render EditDeleteNote correctly', () => {
  const wrapper = shallow(<EditDeleteNote/>);
  expect(wrapper).toMatchSnapshot();
});