import Layout from './MyLayout';
import {shallow} from 'enzyme';

it('should render Layout correctly', () => {
  const wrapper = shallow(<Layout><h1>Testing Layout</h1></Layout>);
  expect(wrapper).toMatchSnapshot();
});