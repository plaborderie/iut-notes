import ConfirmModal from './ConfirmModal';
import {shallow} from 'enzyme';

it('should render ConfirmModal correctly', () => {
  const wrapper = shallow(
    <div>
      <ConfirmModal _id={1} title={'Test Title'} body={'Test Body'}/>
      <button
        data-toggle="modal"
        data-target="#confirmModal-1"
      >
        Supprimer
      </button>
    </div>
  );
  // No modal yet
  expect(wrapper).toMatchSnapshot();

  // Make modal appear
  wrapper.find('button').simulate('click');
  expect(wrapper).toMatchSnapshot();
});