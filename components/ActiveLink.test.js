import {shallow} from 'enzyme';
import Link from './ActiveLink';

it('should render ActiveLink correctly', () => {
  const wrapper = shallow(<Link href="/" activeClassName="active"><a>Hello</a></Link>);
  expect(wrapper).toMatchSnapshot();
});