import Link from "next/link";
import EditDeleteNote from "./EditDeleteNote";

const PostList = (props) =>
  {
    const {posts, user, handleRemovePost} = props;
    return (
      posts === null ? (
          <div className="row justify-content-center">
            <div className="spinner-grow" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        ) :
        posts.length > 0 ? (
          posts.map(post => (
            <React.Fragment key={post._id}>
              <div className="row px-3 justify-content-between">
                <Link href={`/details/${post._id}`}>
                  <a className="h3">{post.title}</a>
                </Link>
                {(user.username === post.author ||
                  user.isAdmin) && (
                  <span>
                      <EditDeleteNote
                        title={`Supprimer "${post.title}" ?`}
                        body="Cette action est irréversible. Une fois la note supprimée, elle est irrécupérable."
                        _id={post._id}
                        onConfirm={() => handleRemovePost(post._id)}
                      />
                    </span>
                )}
              </div>
              <small>Catégorie : {post.category}</small>
              <br/>
              {post.author && <small>Auteur : {post.author}</small>}
              <hr/>
            </React.Fragment>
          ))) : (
          <div className="row justify-content-center">Aucun résultat.</div>
        )
    )
  }

;

export default PostList;