import Link from "next/link";
import ConfirmModal from "./ConfirmModal";

export default props => (
  <div>
    <ConfirmModal
      title={`Supprimer "${props.title}" ?`}
      body="Cette action est irréversible. Une fois la note supprimée, elle est irrécupérable."
      _id={props._id}
      onConfirm={props.onConfirm}
    />
    <Link href={`/edit/${props._id}`}>
      <button className="btn btn-primary">Modifier</button>
    </Link>
    <button
      className="btn btn-danger mx-1"
      data-toggle="modal"
      data-target={`#confirmModal-${props._id}`}
    >
      Supprimer
    </button>
  </div>
)