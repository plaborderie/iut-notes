import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';

const EditUser = props => (
  <Formik
    initialValues={{username: props.user.username, email: props.user.email, password: ''}}
    validationSchema={Yup.object().shape({
      email: Yup.string()
        .email('Adresse e-mail invalide.')
        .required('Pas d\'adresse e-mail spécifiée.'),
      username: Yup.string()
        .required('Pas de nom d\'utilisateur spécifié.'),
      password: Yup.string()
        .min(8, 'Mot de passe trop court - taille minimum de 8 caractères.')
        .required('Pas de mot de passe spécifié.')
      })}
    onSubmit={(values, {setSubmitting}) => props.onSubmit(values, setSubmitting)}
  >
    {formProps => (
      <Form>
        <Field name={'username'} render={({field, form: {touched, errors}}) => (
          <div className="form-group">
            <label htmlFor="username">Nom d'utilisateur</label>
            <input
              {...field}
              type="text"
              className="form-control"
              placeholder="Saisir le nom d'utilisateur"
              className={
                errors[field.name] && touched[field.name] ? 'form-control is-invalid' : 'form-control'
              }
            />
            <ErrorMessage name={"username"} render={msg => <div className={"invalid-feedback"}>{msg}</div>}/>
          </div>
        )}/>
        <Field name={'email'} render={({field, form: {touched, errors}}) => (
          <div className="form-group">
            <label htmlFor="email">Adresse e-mail</label>
            <input
              {...field}
              type="email"
              className="form-control"
              id="email"
              placeholder="Saisir l'adresse e-mail"
              className={
                errors[field.name] && touched[field.name] ? 'form-control is-invalid' : 'form-control'
              }
            />
            <ErrorMessage name={"email"} render={msg => <div className={"invalid-feedback"}>{msg}</div>}/>
          </div>
        )}/>
        <Field name={'password'} render={({field, form: {touched, errors}}) => (
          <div className="form-group">
            <label htmlFor="password">Mot de passe</label>
            <input
              {...field}
              type="password"
              className="form-control"
              id="password"
              placeholder="Saisir le mot de passe"
              className={
                errors[field.name] && touched[field.name] ? 'form-control is-invalid' : 'form-control'
              }
            />
            <ErrorMessage name={"password"} render={msg => <div className={"invalid-feedback"}>{msg}</div>}/>
          </div>
        )}/>
        <Field name={'confirmPassword'} render={({field, form: {touched, errors}}) => (
          <div className="form-group">
            <label htmlFor="confirmPassword">Confirmer mot de passe</label>
            <input
              {...field}
              type="password"
              className="form-control"
              id="confirmPassword"
              placeholder="Saisir le mot de passe"
              className={
                errors[field.name] && touched[field.name] ? 'form-control is-invalid' : 'form-control'
              }
            />
            <ErrorMessage name={"confirmPassword"}
                          render={msg => <div className={"invalid-feedback"}>{msg}</div>}/>
          </div>
        )}/>
        <button type="submit" className="btn btn-primary" disabled={!formProps.isValid}>Confirmer</button>
      </Form>
    )}
  </Formik>
);

export default EditUser;