import Link from "./ActiveLink";
import "../scss/styles.scss";
import { useState } from "react";
import { connect } from "react-redux";

function Header(props) {
  const { user } = props;
  const [toggled, setToggled] = useState(false);

  return (
    <nav className="navbar navbar-expand-lg bg-light text-light">
      <a className="navbar-brand text-dark" href="#">
        IUT Notes
      </a>
      <button
        className={`navbar-toggler ${toggled ? "toggled" : "untoggled"}`}
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
        onClick={() => {
          setToggled(!toggled);
        }}
      >
        <i className="fas fa-bars" />
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link href="/" activeClassName="active">
              <a className="nav-link">Accueil</a>
            </Link>
          </li>
          <li className="nav-item">
            <Link href="/post" activeClassName="active">
              <a className="nav-link">Notes</a>
            </Link>
          </li>
          {!user || !user.username ? (
            <React.Fragment>
              <li className="nav-item">
                <Link href="/user/new" activeClassName="active">
                  <a className="nav-link">Créer un compte</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/user/login" activeClassName="active">
                  <a className="nav-link">Se connecter</a>
                </Link>
              </li>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <li className="nav-item">
                <Link href="/post/new" activeClassName="active">
                  <a className="nav-link">Nouvelle note</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/user" activeClassName="active">
                  <a className="nav-link">Mon compte ({user.username})</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/user/logout" activeClassName="active">
                  <a className="nav-link">Se déconnecter</a>
                </Link>
              </li>
            </React.Fragment>
          )}
        </ul>
      </div>
    </nav>
  );
}

export default connect(state => state)(Header);
