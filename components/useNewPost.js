import { useState, useEffect } from "react";
import axios from "axios";

function useNewPost(props) {
  const [post, setPost] = useState({
    success: false,
    categories: [],
    semesters: ["S1", "S2", "S3", "IPI", "PEL"],
    markdown: "",
    jwt: "",
    statusCode: null,
    title: "",
    category: ""
  });

  useEffect(() => {
    init();
  }, []);

  async function init() {
    const jwt = localStorage.getItem("jwt");
    const statusCode = jwt ? null : 401;
    let categories;
    try {
      const { data } = await axios.get(`/categories`);
      categories = data;
    } catch (e) {
      throw e;
    }
    const { title, category, content } = props
      ? props.post
      : {
          title: "",
          category: "",
          content: ""
        };
    setPost({
      ...post,
      title,
      category,
      markdown: content,
      jwt,
      statusCode,
      categories
    });
  }

  return [post, setPost];
}

export default useNewPost;
