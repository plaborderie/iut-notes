export default props => (
  <div className="modal fade" id={`confirmModal-${props._id}`} tabindex="-1" role="dialog" aria-labelledby="confirmModal"
       aria-hidden="true">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">{props.title}</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          {props.body}
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Fermer</button>
          <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={props.onConfirm}>Confirmer</button>
        </div>
      </div>
    </div>
  </div>
)