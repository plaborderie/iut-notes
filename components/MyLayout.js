import Header from "./Header";
import Head from "next/head";
import "../scss/styles.scss";

const Layout = props => (
  <div className="w-100 h-100">
    <Head>
      <title>IUT Notes</title>
    </Head>
    <Header />
    <div className="container-fluid px-0">
      <div className="row justify-content-center no-gutters w-100">
        <div className="mx-0 my-2 p-3 bg-light text-dark col col-xs-12 col-md-6">
          {props.children}
        </div>
      </div>
    </div>
  </div>
);

export default Layout;
