const { PHASE_PRODUCTION_SERVER } =
  process.env.NODE_ENV === "development" ? {} : require("next/constants");

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_PRODUCTION_SERVER) {
    // Config used to run in production.
    return {};
  }

  const withCss = require("@zeit/next-css");
  const withSass = require("@zeit/next-sass");

  return withCss(withSass());
};
