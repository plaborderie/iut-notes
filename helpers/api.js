import axios from "axios";
import Router from "next/dist/lib/router";
import { getUser } from "../actions/authActions";

export const getPosts = async () => {
  try {
    const { data } = await axios.get(`/posts`);
    return data;
  } catch (err) {
    throw err;
  }
};

export const getUserPosts = async author => {
  try {
    const { data } = await axios.post("/posts/user", { author });
    return data;
  } catch (err) {
    throw err;
  }
};

export const removePost = async id => {
  const jwt = localStorage.getItem("jwt");
  try {
    const headers = {
      headers: { Authorization: `bearer ${jwt}` }
    };
    const { data } = await axios.delete(`/posts/${id}`, headers);
    return data;
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const getCategories = async () => {
  try {
    const { data } = await axios.get(`/categories`);
    return data;
  } catch (err) {
    console.error(err);
  }
};

export const editUser = async (username, user) => {
  const jwt = localStorage.getItem("jwt");
  try {
    const headers = {
      headers: { Authorization: `bearer ${jwt}` }
    };
    const body = { user, username };
    const { data } = await axios.put("/users", body, headers);
    return data;
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const login = async ({ email, password }, dispatch) => {
  const user = {
    email,
    password
  };
  const res = await axios.post(`/users/login`, user);
  const jwt = res.data.token;
  localStorage.setItem("jwt", jwt);
  // Update user state
  dispatch(getUser());
  // Go back to homepage
  Router.push("/");
};
