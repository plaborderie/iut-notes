const sgMail = require('@sendgrid/mail');

const passwordResetMail = (to, token, host) => {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    from: 'password-reset@iut-notes.com',
    to,
    subject: `Demande de changement de mot de passe IUT Notes`,
    text: `Vous recevez ce mail car vous (ou quelqu'un d'autre) a demandé la réinitialisation du mot de passe de votre compte. 
          Cliquez sur le lien suivant ou copiez-le dans la barre de recherche de votre navigateur pour changer de mot de passe :
          https://${host}/reset/${token}
          Si vous n'êtes pas à l'origine de cette demande, ignorez ce message. Votre mot de passe ne sera pas modifié.`
  };
  sgMail.send(msg);
};

const passwordChangedMail = to => {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    from: 'password-reset@iut-notes.com',
    to,
    subject: `Mot de passe modifié !`,
    text: `Votre mot de passe a été modifié avec succès.`
  };
  sgMail.send(msg);
};

module.exports = { passwordResetMail, passwordChangedMail };