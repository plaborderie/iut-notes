import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import reducer from "../reducers/authReducer";

export const initialState = { user: { isAdmin: false, username: "" } };

const makeStore = (initialState, options) => {
  return createStore(reducer, initialState, applyMiddleware(thunkMiddleware));
};

export default makeStore;
