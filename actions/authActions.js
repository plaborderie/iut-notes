import axios from "axios";

export const getUser = () => {
  return dispatch => {
    const jwt = localStorage.getItem("jwt");
    if (jwt !== null) {
      const headers = {
        headers: { Authorization: `bearer ${jwt}` }
      };
      axios
        .get("/users/jwt", headers)
        .then(({ data }) => data)
        .then(payload => {
          return dispatch({ type: "SET_USER", payload });
        });
    }
  };
};
