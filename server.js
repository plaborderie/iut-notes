require("newrelic");

const express = require("express");
const next = require("next");

const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const userRouter = require("./models/users/routes");
const postRouter = require("./models/posts/routes");
const categoriesRouter = require("./models/categories/routes");
const cors = require("cors");
require("dotenv").config();
require("./passport");

const mongoUrl = dev
  ? "mongodb://localhost/iut-notes-dev"
  : process.env.MLAB_URI;
// Connecting to MongoDB
const mongoose = require("mongoose");
mongoose.connect(mongoUrl);

app
  .prepare()
  .then(() => {
    const server = express();

    server.use(express.json());

    server.use(cors());

    server.options("*", cors());

    // Enabling forestadmin
    server.use(
      require("forest-express-mongoose").init({
        modelsDir: __dirname + "/models",
        envSecret: process.env.FOREST_ENV_SECRET,
        authSecret: process.env.FOREST_AUTH_SECRET,
        mongoose: require("mongoose")
      })
    );

    const faviconOptions = {
      root: __dirname + "/static/"
    };

    server.get("/favicon.ico", (req, res) =>
      res.status(200).sendFile("favicon.ico", faviconOptions)
    );

    server.use("/users", userRouter);

    server.use("/posts", postRouter);

    server.use("/categories", categoriesRouter);

    server.get("/details/:id", (req, res) => {
      const actualPage = "/post/post";
      const queryParams = { id: req.params.id };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/reset/:token", (req, res) => {
      const actualPage = "/user/reset";
      const queryParams = { token: req.params.token };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/edit/:id", async (req, res) => {
      const actualPage = "/post/edit";
      const queryParams = { id: req.params.id };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(PORT, err => {
      if (err) throw err;
      console.log("> Ready on http://localhost:" + PORT);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
