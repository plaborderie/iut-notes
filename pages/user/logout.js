import { connect } from "react-redux";
import Layout from "../../components/MyLayout";
import Link from "next/link";
import Router from "next/router";
import { useEffect } from "react";
import { getUser } from "../../actions/authActions";

function Logout({ dispatch }) {
  const handleLogout = () => {
    if (localStorage.getItem("jwt") !== null) {
      localStorage.removeItem("jwt");
    }
    dispatch(getUser());
    Router.push("/");
  };

  useEffect(() => {
    handleLogout();
  }, []);

  return (
    <Layout>
      <h1>Déconnexion</h1>
      <p>Vous avez été déconnecté avec succès !</p>
      <Link href="/">
        <a>Retourner à l'accueil</a>
      </Link>
    </Layout>
  );
}
export default connect(state => state)(Logout);
