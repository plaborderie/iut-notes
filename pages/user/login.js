import Layout from "../../components/MyLayout";
import axios from "axios";
import Router from "next/router";
import { Alert } from "reactstrap";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { useState } from "react";

function Login() {
  const [status, setStatus] = useState(0);

  const handleLogin = async (values, setSubmitting) => {
    const { email, password } = values;

    const user = {
      email,
      password
    };

    try {
      const res = await axios.post(`/users/login`, user);
      const jwt = res.data.token;
      localStorage.setItem("jwt", jwt);
      setSubmitting(false);
      Router.push("/");
    } catch (err) {
      setStatus(err.response.status);
    }
  };

  const resetStatus = () => {
    setStatus(0);
  };

  return (
    <Layout>
      <h1>Connexion</h1>
      <div>
        <Alert color="danger" isOpen={status === 400} toggle={resetStatus}>
          Email/mot de passe erroné
        </Alert>
        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Adresse e-mail invalide.")
              .required("Pas d'adresse e-mail spécifiée."),
            password: Yup.string()
              .min(
                8,
                "Mot de passe trop court - taille minimum de 8 caractères."
              )
              .required("Pas de mot de passe spécifié.")
          })}
          onSubmit={(values, { setSubmitting }) =>
            handleLogin(values, setSubmitting)
          }
        >
          {props => (
            <Form>
              <Field
                name={"email"}
                render={({ field, form: { touched, errors } }) => (
                  <div className="form-group">
                    <label htmlFor="email">Adresse e-mail</label>
                    <input
                      {...field}
                      type="email"
                      className="form-control"
                      id="email"
                      placeholder="Saisir l'adresse e-mail"
                      className={
                        errors[field.name] && touched[field.name]
                          ? "form-control is-invalid"
                          : "form-control"
                      }
                    />
                    <ErrorMessage
                      name={"email"}
                      render={msg => (
                        <div className={"invalid-feedback"}>{msg}</div>
                      )}
                    />
                  </div>
                )}
              />
              <Field
                name={"password"}
                render={({ field, form: { touched, errors } }) => (
                  <div className="form-group">
                    <label htmlFor="password">Mot de passe</label>
                    <input
                      {...field}
                      type="password"
                      className="form-control"
                      id="password"
                      placeholder="Saisir le mot de passe"
                      className={
                        errors[field.name] && touched[field.name]
                          ? "form-control is-invalid"
                          : "form-control"
                      }
                    />
                    <ErrorMessage
                      name={"password"}
                      render={msg => (
                        <div className={"invalid-feedback"}>{msg}</div>
                      )}
                    />
                  </div>
                )}
              />
              <button
                type="submit"
                className="btn btn-primary"
                disabled={!props.isValid}
              >
                Confirmer
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </Layout>
  );
}

export default Login;
