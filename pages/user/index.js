import { useState, useEffect } from "react";
import { connect } from "react-redux";
import Layout from "../../components/MyLayout";
import PostList from "../../components/PostList";
import EditUser from "../../components/EditProfile";
import { getUserPosts, removePost, editUser, login } from "../../helpers/api";

function Index(props) {
  const { user } = props;

  const [posts, setPosts] = useState(null);
  const [tabs, setTabs] = useState([
    { title: "Mes notes", value: "PostList" },
    { title: "Modifier profil", value: "EditProfile" }
  ]);
  const [selectedTab, setSelectedTab] = useState(tabs[0].value);

  useEffect(() => {
    handleGetPosts();
  }, [user]);

  const handleGetPosts = async () => {
    try {
      const result = await getUserPosts(user.username);
      setPosts(result);
    } catch (err) {
      console.error(err);
    }
  };

  const handleRemovePost = async id => {
    try {
      const deleted = await removePost(id);
      const filtered = posts.filter(post => post._id !== deleted._id);
      setPosts(filtered);
    } catch (err) {
      console.error(err);
    }
  };

  const onSubmit = async (values, setSubmitting) => {
    try {
      const username = user.username;
      const newUser = values;
      await editUser(username, newUser);
      setSubmitting(false);
      await login(newUser, props.dispatch);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Layout>
      <h1>{user.username}</h1>
      <hr />
      <ul className="nav nav-pills">
        {tabs.map(tab => (
          <li className="nav-item" key={tab.title}>
            <button
              className={`btn ${
                selectedTab === tab.value
                  ? "btn-outline-primary active"
                  : "btn-outline-primary"
              }`}
              onClick={() => setSelectedTab(tab.value)}
            >
              {tab.title}
            </button>
          </li>
        ))}
      </ul>
      <br />
      {selectedTab === "PostList" ? (
        <PostList
          posts={posts}
          user={user}
          handleRemovePost={handleRemovePost}
        />
      ) : (
        selectedTab === "EditProfile" && (
          <EditUser user={user} onSubmit={onSubmit} />
        )
      )}
    </Layout>
  );
}

export default connect(state => state)(Index);
