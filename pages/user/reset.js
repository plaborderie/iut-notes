import Layout from "../../components/MyLayout";
import axios from "axios";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { Alert } from "reactstrap";
import Link from "next/link";
import { useState } from "react";

function Reset(props) {
  const [success, setSuccess] = useState(false);
  const [status, setStatus] = useState(0);

  const onResetPassword = async (values, setSubmitting, resetForm) => {
    setSubmitting(false);
    const { password } = values;
    const jwt = localStorage.getItem("jwt") || "";
    const headers = {
      headers: { Authorization: `bearer ${jwt}` }
    };

    try {
      const res = await axios.post(
        "/users/reset",
        { password, token: props.token },
        headers
      );
      setSuccess(true);
      resetForm();
    } catch (err) {
      setStatus(err.response.status);
    }
  };

  const resetStatus = () => {
    setStatus(0);
  };

  return (
    <Layout>
      <Alert
        isOpen={success}
        toggle={() => {
          setSuccess(!success);
        }}
      >
        Mot de passe modifié avec succès !
        <br />
        <Link href="/user/login">
          <a>Se connecter</a>
        </Link>
      </Alert>
      <Alert color="danger" isOpen={status === 500} toggle={resetStatus}>
        Erreur interne.
      </Alert>
      <h1>Réinitialisation du mot de passe</h1>
      <Formik
        initialValues={{ password: "", confirmPassword: "" }}
        validationSchema={Yup.object().shape({
          password: Yup.string()
            .min(8, "Mot de passe trop court - taille minimum de 8 caractères.")
            .required("Pas de mot de passe spécifié."),
          confirmPassword: Yup.string().oneOf(
            [Yup.ref("password"), null],
            "Les mots de passes ne correspondent pas."
          )
        })}
        onSubmit={(values, { setSubmitting, resetForm }) =>
          onResetPassword(values, setSubmitting, resetForm)
        }
      >
        {props => (
          <Form>
            <Field
              name={"password"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="password">Mot de passe</label>
                  <input
                    {...field}
                    type="password"
                    className="form-control"
                    id="password"
                    placeholder="Saisir le mot de passe"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"password"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <Field
              name={"confirmPassword"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="confirmPassword">
                    Confirmer mot de passe
                  </label>
                  <input
                    {...field}
                    type="password"
                    className="form-control"
                    id="confirmPassword"
                    placeholder="Saisir le mot de passe"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"confirmPassword"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!props.isValid}
            >
              Confirmer
            </button>
          </Form>
        )}
      </Formik>
    </Layout>
  );
}

Reset.getInitialProps = ctx => {
  const { token } = ctx.query;

  return { token };
};

export default Reset;
