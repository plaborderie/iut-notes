import Layout from "../../components/MyLayout";
import axios from "axios";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { Alert } from "reactstrap";
import { useState } from "react";

export default function Forgot() {
  const [success, setSuccess] = useState(false);
  const [status, setStatus] = useState(0);

  const sendResetEmail = async (values, setSubmitting) => {
    try {
      const { email } = values;
      const res = await axios.post(`/users/forgot`, { email });
      setSubmitting(false);
      setSuccess(true);
    } catch (err) {
      console.error(err.response);
      setStatus(err.response.status);
    }
  };

  const resetStatus = () => {
    setStatus(0);
  };

  return (
    <Layout>
      <Alert
        isOpen={success}
        toggle={() => {
          setSuccess(!success);
        }}
      >
        Surveillez votre boîte mail pour réinitialiser votre mot de passe.
      </Alert>
      <Alert color="danger" isOpen={status === 400} toggle={resetStatus}>
        Aucun compte associé à cette adresse e-mail
      </Alert>
      <Alert color="danger" isOpen={status === 500} toggle={resetStatus}>
        Erreur interne.
      </Alert>
      <h1>Réinitialisation du mot de passe</h1>
      <Formik
        initialValues={{ password: "", confirmPassword: "" }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email("Adresse e-mail invalide.")
            .required("Pas d'adresse e-mail spécifiée.")
        })}
        onSubmit={(values, { setSubmitting }) =>
          sendResetEmail(values, setSubmitting)
        }
      >
        {props => (
          <Form>
            <Field
              name={"email"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="email">Adresse e-mail</label>
                  <input
                    {...field}
                    type="email"
                    className="form-control"
                    id="email"
                    placeholder="Saisir l'adresse e-mail"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"email"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!props.isValid}
            >
              Confirmer
            </button>
          </Form>
        )}
      </Formik>
    </Layout>
  );
}
