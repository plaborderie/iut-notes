import Layout from "../../components/MyLayout";
import axios from "axios";
import { Alert } from "reactstrap";
import Router from "next/router";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";

import { useState } from "react";

function NewUser() {
  const [userExists, setUserExists] = useState(false);
  const [internalError, setInternalError] = useState(false);

  const handleCreateUser = async (values, setSubmitting) => {
    const { username, password, email } = values;

    const user = { username, password, email };

    try {
      const res = await axios.post(`/users`, user);
      setSubmitting(false);
      login(user);
    } catch (err) {
      console.error(err);
      if (err.response.status === 409) {
        setUserExists(true);
      } else if (err.response.status === 500) {
        setInternalError(true);
      }
    }
  };

  const login = async ({ email, password }) => {
    const user = {
      email,
      password
    };
    const res = await axios.post(`/users/login`, user);
    const jwt = res.data.token;
    localStorage.setItem("jwt", jwt);
    Router.push("/");
  };

  const resetData = () => {
    document.getElementById("username").value = "";
    document.getElementById("password").value = "";
  };

  const changeAlertExists = () => {
    setUserExists(!userExists);
  };

  const changeAlertInternal = () => {
    setInternalError(!internalError);
  };

  return (
    <Layout>
      <Alert color="danger" isOpen={userExists} toggle={changeAlertExists}>
        L'utilisateur existe déjà.
      </Alert>
      <Alert color="danger" isOpen={internalError} toggle={changeAlertInternal}>
        Erreur interne. Veuillez réessayer plus tard.
      </Alert>
      <h1>Créer un compte</h1>
      <Formik
        initialValues={{
          username: "",
          email: "",
          password: "",
          confirmPassword: ""
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email("Adresse e-mail invalide.")
            .required("Pas d'adresse e-mail spécifiée."),
          username: Yup.string().required("Pas de nom d'utilisateur spécifié."),
          password: Yup.string()
            .min(8, "Mot de passe trop court - taille minimum de 8 caractères.")
            .required("Pas de mot de passe spécifié."),
          confirmPassword: Yup.string().oneOf(
            [Yup.ref("password"), null],
            "Les mots de passes ne correspondent pas."
          )
        })}
        onSubmit={(values, { setSubmitting }) =>
          handleCreateUser(values, setSubmitting)
        }
      >
        {props => (
          <Form>
            <Field
              name={"username"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="username">Nom d'utilisateur</label>
                  <input
                    {...field}
                    type="text"
                    className="form-control"
                    placeholder="Saisir le nom d'utilisateur"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"username"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <Field
              name={"email"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="email">Adresse e-mail</label>
                  <input
                    {...field}
                    type="email"
                    className="form-control"
                    id="email"
                    placeholder="Saisir l'adresse e-mail"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"email"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <Field
              name={"password"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="password">Mot de passe</label>
                  <input
                    {...field}
                    type="password"
                    className="form-control"
                    id="password"
                    placeholder="Saisir le mot de passe"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"password"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <Field
              name={"confirmPassword"}
              render={({ field, form: { touched, errors } }) => (
                <div className="form-group">
                  <label htmlFor="confirmPassword">
                    Confirmer mot de passe
                  </label>
                  <input
                    {...field}
                    type="password"
                    className="form-control"
                    id="confirmPassword"
                    placeholder="Saisir le mot de passe"
                    className={
                      errors[field.name] && touched[field.name]
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                  <ErrorMessage
                    name={"confirmPassword"}
                    render={msg => (
                      <div className={"invalid-feedback"}>{msg}</div>
                    )}
                  />
                </div>
              )}
            />
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!props.isValid}
            >
              Confirmer
            </button>
          </Form>
        )}
      </Formik>
    </Layout>
  );
}

export default NewUser;
