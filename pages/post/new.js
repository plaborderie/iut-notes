import Layout from "../../components/MyLayout";
import axios from "axios";
import { Alert } from "reactstrap";
import SimpleMDE from "react-simplemde-editor";
import "simplemde/dist/simplemde.min.css";
import Error from "next/error";
import { useState, useEffect } from "react";

import useNewPost from "../../components/useNewPost";

function NewPost() {
  const [post, setPost] = useNewPost();
  const [isValid, setIsValid] = useState(false);

  // Set initial selected category
  // And on change semester
  useEffect(() => {
    const available = post.categories.find(
      category => category.semester === post.semester
    );
    if (available) {
      setPost({ ...post, category: available.title });
    }
  }, [post.categories, post.semester]);

  useEffect(() => {
    setIsValid(
      post.title.length > 0 &&
        post.category.length > 0 &&
        post.markdown.length > 0
    );
  }, [post]);

  const resetFormContent = () => {
    setPost({ ...post, content: "", title: "" });
  };

  const handleAddPost = async () => {
    const { title, category, markdown: content } = post;
    const newPost = {
      title,
      category,
      content
    };

    const headers = {
      headers: { Authorization: `bearer ${post.jwt}` }
    };

    try {
      const res = await axios.post(`/posts`, newPost, headers);
      changeAlert();
      resetFormContent();
    } catch (err) {
      console.error(err);
      setPost({ ...post, statusCode: err.status });
    }
  };

  const onChange = e => {
    setPost({ ...post, [e.target.name]: e.target.value });
  };

  const handleChangeMarkdown = markdown => {
    setPost({ ...post, markdown });
  };

  const changeAlert = () => {
    setPost({ ...post, success: !post.success });
  };

  if (post.statusCode) {
    return <Error statusCode={post.statusCode} />;
  }
  return (
    <Layout>
      <Alert isOpen={post.success} toggle={changeAlert}>
        Note enregistrée avec succès !
      </Alert>

      <h1>Nouvelle note</h1>
      <div>
        <div className="form-group">
          <label htmlFor="postTitle">Titre de la note</label>
          <input
            type="text"
            name="title"
            value={post.title}
            onChange={onChange}
            className="form-control"
            id="postTitle"
            placeholder="Saisir le titre"
          />
        </div>
        <div className="form-group">
          <label for="postSemester">Semestre concerné</label>
          <select
            id="postSemester"
            className="form-control"
            name="semester"
            onChange={onChange}
          >
            {post.semesters.map(semester => (
              <option value={semester} key={semester}>
                {semester}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="postCategory">Catégorie de la note</label>
          <select
            id="postCategory"
            className="form-control"
            name="category"
            onChange={onChange}
          >
            {post.categories.map(
              category =>
                category.semester === post.semester && (
                  <option value={category.title} key={category._id}>
                    {category.title}
                  </option>
                )
            )}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="postContent">Contenu</label>
          <SimpleMDE
            id="postContent"
            onChange={handleChangeMarkdown}
            options={{
              spellChecker: false
            }}
            value={post.content}
          />
        </div>
        <button
          onClick={() => handleAddPost()}
          disabled={!isValid}
          className="btn btn-primary"
        >
          Confirmer
        </button>
      </div>
    </Layout>
  );
}

export default NewPost;
