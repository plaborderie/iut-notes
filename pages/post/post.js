import { connect } from "react-redux";
import axios from "axios";
import ReactMarkdown from "react-markdown";
import Layout from "../../components/MyLayout";
import EditDeleteNote from "../../components/EditDeleteNote";
import ReactToPrint from "react-to-print";
import Router from "next/router";

let componentRef;

class ComponentToPrint extends React.Component {
  render() {
    return (
      <div id="note">
        <ReactMarkdown source={this.props.post.content} />
      </div>
    );
  }
}

const Post = props => {
  const { user } = props;

  const handleRemovePost = async id => {
    const jwt = localStorage.getItem("jwt");
    try {
      const headers = {
        headers: { Authorization: `bearer ${jwt}` }
      };
      const res = await axios.delete(`/posts/${id}`, headers);
      const deleted = res.data;
      //  Redirect
      Router.push("/");
    } catch (err) {
      console.error(err);
    }
  };
  return (
    <Layout>
      <h1>{props.post.title}</h1>
      <small>Catégorie : {props.post.category}</small>
      <br />
      {props.post.author && <small>Auteur : {props.post.author}</small>}
      <br />
      {(user.username === props.post.author || user.isAdmin) && (
        <React.Fragment>
          <EditDeleteNote
            title={props.post.title}
            body="Cette action est irréversible. Une fois la note supprimée, elle est irrécupérable."
            _id={props.post._id}
            onConfirm={() => handleRemovePost(props.post._id)}
          />
          <br />
        </React.Fragment>
      )}
      <ReactToPrint
        trigger={() => (
          <button className="btn btn-primary">
            <i className="fas fa-print pr-2" />
            Imprimer
          </button>
        )}
        content={() => componentRef}
        bodyClass="print-style"
      />
      <hr />
      <ComponentToPrint ref={el => (componentRef = el)} post={props.post} />
    </Layout>
  );
};

Post.getInitialProps = async ctx => {
  const { id } = ctx.query;
  let post = {};
  try {
    const instance = axios.create({
      baseURL:
        process.env.NODE_ENV !== "production"
          ? "http://localhost:3000"
          : "https://iut-notes-next.herokuapp.com"
    });

    const res = await instance.get(`/posts/${id}`);
    post = res.data;
  } catch (err) {
    console.error(err.response);
  }
  return { post };
};

export default connect(state => state)(Post);
