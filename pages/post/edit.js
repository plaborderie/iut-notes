import axios from "axios";
import { Alert } from "reactstrap";
import SimpleMDE from "react-simplemde-editor";
import "simplemde/dist/simplemde.min.css";
import Error from "next/error";
import Layout from "../../components/MyLayout";

import useNewPost from "../../components/useNewPost";

function EditPost(props) {
  const [post, setPost] = useNewPost(props);
  const resetFormContent = () => {
    setPost({ ...post, markdown: "", title: "" });
  };

  const handleAddPost = async () => {
    const title = document.getElementById("postTitle").value;
    const { markdown, category } = post;
    const { _id, author } = props.post;

    const newPost = {
      title,
      category,
      content: markdown,
      _id,
      author
    };

    const headers = {
      headers: { Authorization: `bearer ${post.jwt}` }
    };

    try {
      console.log(headers);
      const res = await axios.put(`/posts`, newPost, headers);
      changeAlert();
      resetFormContent();
    } catch (err) {
      console.error(err);
      setPost({ ...post, statusCode: err.status });
    }
  };

  const handleChangeMarkdown = markdown => {
    setPost({ ...post, markdown });
  };

  const changeCategory = e => {
    const category = e.target.value;
    setPost({ ...post, category });
  };

  const changeTitle = e => {
    const title = e.target.value;
    setPost({ ...post, title });
  };

  const changeAlert = () => {
    setPost({ ...post, success: !post.success });
  };

  if (post.statusCode) {
    return <Error statusCode={post.statusCode} />;
  }
  return (
    <Layout>
      <Alert isOpen={post.success} toggle={changeAlert}>
        Note enregistrée avec succès !
      </Alert>

      <h1>Nouvelle note</h1>
      <div>
        <div className="form-group">
          <label htmlFor="postTitle">Titre de la note</label>
          <input
            type="text"
            className="form-control"
            id="postTitle"
            placeholder="Saisir le titre"
            value={post.title}
            onChange={changeTitle}
          />
        </div>
        <div className="form-group">
          <label htmlFor="postCategory">Catégorie de la note</label>
          <select
            id="postCategory"
            value={post.category}
            onChange={changeCategory}
            className="form-control"
          >
            {post.categories.map(category => (
              <option value={category.title} key={category._id}>
                {category.title}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="postContent">Contenu</label>
          <SimpleMDE
            id="postContent"
            onChange={handleChangeMarkdown}
            options={{
              spellChecker: false
            }}
            value={post.markdown}
          />
        </div>
        <button onClick={() => handleAddPost()} className="btn btn-primary">
          Confirmer
        </button>
      </div>
    </Layout>
  );
}

EditPost.getInitialProps = async ctx => {
  const { id } = ctx.query;
  let post = {};
  try {
    const instance = axios.create({
      baseURL:
        process.env.NODE_ENV !== "production"
          ? "http://localhost:3000"
          : "https://iut-notes-next.herokuapp.com"
    });
    const res = await instance.get(`/posts/${id}`);
    post = res.data;
  } catch (err) {
    console.error(err.response);
  }
  return { post };
};

export default EditPost;
