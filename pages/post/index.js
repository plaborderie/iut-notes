import { connect } from "react-redux";
import Layout from "../../components/MyLayout.js";
import { getCategories, getPosts, removePost } from "../../helpers/api";
import { useState, useEffect } from "react";
import PostList from "../../components/PostList";

function Post(props) {
  const [data, setData] = useState({
    posts: null,
    categories: [],
    semesters: ["S1", "S2", "S3", "IPI", "PEL"],
    semester: "Tout",
    selected: "Tout",
    search: ""
  });

  const [posts, setPosts] = useState(null);
  const [categories, setCategories] = useState([]);
  const [availablePosts, setAvailablePosts] = useState(null);
  const [availableCategories, setAvailableCategories] = useState([]);
  const { user } = props;

  useEffect(() => {
    handleGetCategories();
    handleGetPosts();
  }, []);

  useEffect(() => {
    setAvailablePosts(getAvailablePosts());
    setAvailableCategories(getAvailableCategories());
  }, [data, posts, categories]);

  const handleGetPosts = async () => {
    try {
      const result = await getPosts();
      setPosts(result);
    } catch (err) {
      console.error(err);
    }
  };

  const handleRemovePost = async id => {
    try {
      const deleted = await removePost(id);
      const result = posts.filter(post => post._id !== deleted._id);
      setPosts(result);
    } catch (err) {
      console.error(err);
    }
  };

  const handleGetCategories = async () => {
    try {
      const result = await getCategories();
      setCategories(result);
    } catch (err) {
      console.error(err);
    }
  };

  const handleSelectCategory = e => {
    setData({ ...data, selected: e.target.value });
  };

  const handleSearchTitle = e => {
    setData({ ...data, search: e.target.value });
  };

  const onChangeSemester = e => {
    setData({ ...data, semester: e.target.value });
  };

  const getAvailableCategories = () => {
    if (data.semester === "Tout") {
      return categories;
    } else {
      return categories.filter(cat => {
        return cat.semester === data.semester;
      });
    }
  };

  const getAvailablePosts = () => {
    if (posts === null) {
      return [];
    }
    let result = [];
    if (data.selected === "Tout" && data.semester !== "Tout") {
      result = posts.filter(post => {
        const category = categories.find(cat => cat.title === post.category);
        if (category.semester === data.semester) {
          return post;
        }
      });
    } else if (data.selected === "Tout") {
      result = posts;
    } else {
      result = posts.filter(post => post.category === data.selected);
    }

    // Apply text filter
    result = result.filter(res =>
      res.title.toLowerCase().includes(data.search.toLowerCase())
    );
    return result;
  };

  return (
    <Layout>
      <div>
        <div className="form-group">
          <label htmlFor="postSemester">Semestre concerné</label>
          <select
            id="postSemester"
            className="form-control"
            name="semester"
            onChange={onChangeSemester}
          >
            <option selected>Tout</option>
            {data.semesters.map(semester => (
              <option value={semester} key={semester}>
                {semester}
              </option>
            ))}
          </select>
        </div>
        <label htmlFor="postCategory">Catégorie de la note</label>
        <select
          id="postCategory"
          className="form-control"
          onChange={handleSelectCategory}
          value={data.value}
        >
          <option selected>Tout</option>
          {availableCategories.map(category => (
            <option value={category.title} key={category._id}>
              {category.title}
              {data.semester === "Tout" && ` - ${category.semester}`}
            </option>
          ))}
        </select>
        <br />
        <label htmlFor="postTitle">Titre</label>
        <input
          type="text"
          className="form-control"
          id="postTitle"
          placeholder="Saisir le titre"
          onChange={handleSearchTitle}
        />
      </div>
      <br />
      <hr />
      <PostList
        posts={availablePosts}
        user={user}
        handleRemovePost={handleRemovePost}
      />
    </Layout>
  );
}

export default connect(state => state)(Post);
