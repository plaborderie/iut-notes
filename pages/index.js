import Layout from "../components/MyLayout.js";

export default () => (
  <Layout>
    <h1>Bienvenue sur IUT Notes !</h1>
    <p className="lead">
      IUT Notes est une plateforme d'échange de notes de cours ou d'exercices à
      l'IUT Informatique de Bayonne et du Pays Basque.
    </p>
  </Layout>
);
